//    sequencer1394.h - ieee-1394 (firewire) frame sequencer
//
//    Copyright (C) 2005  Adam McCullough <adam.mccullough@mansfield.oxon.org>
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef CT_SEQUENCER_1394
#define CT_SEQUENCER_1394

#include <libraw1394/raw1394.h>
#include <libdc1394/dc1394_control.h>

#include "sequencer.h"

#define MAX_PORTS 4
#define MAX_RESETS 10
#define FRAME_CAPTURE_MAX_FAILURES 10
#define DMA_DEVICE_FILE "/dev/video0"


namespace CamTrack{

  class Sequencer1394: public Sequencer{

    public:
      Sequencer1394(const u_int64_t guid);
      ~Sequencer1394();
      
      int reset();
      int start_stream();
      long grab_frame();
      int stop_stream();
           
      Imlib_Image *get_image();

    protected:
    
      Imlib_Image im;
      
      dc1394_cameracapture camera;
      raw1394handle_t handle;
      unsigned long first_frame;
      unsigned long last_frame;
      unsigned long current_frame;
      
  };

};

#endif
