//    mstracker.h - mean shift tracker
//
//    Copyright (C) 2005  Adam McCullough <adam.mccullough@mansfield.oxon.org>
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef CT_MSTRACKER
#define CT_MSTRACKER

#include <inttypes.h>

#include "probmap.h"
#include "window.h"

namespace CamTrack{

  #define MIN_WINDOW_SIZE 81
  #define CONVERGENCE_DIST 3
  #define WINDOW_ASPECT 1.2
  #define WIN_SIZE_FACTOR 1.15

  class MeanShiftTracker{

    public:
      MeanShiftTracker(ProbMap& rmap, TrackingWindow& rwin);
      ~MeanShiftTracker();
      
      void track();
      void reset();
      
      bool is_tracking(){return tracking;}
      bool is_bounding(){return bounding;}
      
    protected:
    
      ProbMap& map;
      TrackingWindow& win;
    
      bool tracking;
      bool bounding;
      unsigned long framenumber;
            
      float aspect, size_factor;
      float density_threshold;
      
  };

};

#endif
