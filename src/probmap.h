//    probmap.h - probability map
//
//    Copyright (C) 2005  Adam McCullough <adam.mccullough@mansfield.oxon.org>
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef CT_PROBMAP
#define CT_PROBMAP

#include <X11/Xlib.h>
#include <Imlib2.h>
#include <inttypes.h>

#include "histogram.h"
#include "window.h"

namespace CamTrack{

  class ProbMap{

    public:
      ProbMap(unsigned int width, unsigned int height);
      ~ProbMap();
      
      int scan(Imlib_Image *image, Histogram *hist, TrackingWindow &win);
      int close();
      
      unsigned int get_pixel(register int x, register int y){
        return srcmap[y][x>>5] & (0x80000000 >> (x & 0x0000001F));
      }
      
      void set_eye_region(TrackingWindow &win);
      
      unsigned int get_width(){return width;}
      unsigned int get_height(){return height;}

      int eyeregion_l, eyeregion_r, eyeregion_t;


    protected:
      
      unsigned int set_pixel(int x, int y, int val);
      void set_block(int x, int y, uint32_t val){
        srcmap[y][x>>5] = val;
      }
      
      unsigned int width, height;
      uint32_t **srcmap;
      uint32_t **dstmap;
      
      uint32_t *buffer1;
      uint32_t *buffer2;
      
      int dilate();
      void erode();
            
      unsigned int buffersize;
      unsigned int row_size;
            
      void swap_buffers();
      int track_zero(int *row1, int *row2, int i, int d);
            
  };

};

#endif
