//    sequencerV4L.h - video4linux frame sequencer
//
//    Copyright (C) 2005  Adam McCullough <adam.mccullough@mansfield.oxon.org>
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef CT_SEQUENCER_V4L
#define CT_SEQUENCER_V4L

#include <linux/types.h>
#include <linux/videodev.h>

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif
#include <jpeglib.h>
#ifdef __cplusplus
} // extern "C"
#endif

#include "sequencer.h"

typedef struct {
  struct jpeg_source_mgr pub;
  
  size_t data_size;
  
  JOCTET *buffer;
  bool start;
} buffer_source_mgr;

typedef buffer_source_mgr * buffer_src_ptr;


namespace CamTrack{

  class SequencerV4L: public Sequencer{

    public:
      SequencerV4L();
      ~SequencerV4L();
      
      int reset();
      int start_stream();
      long grab_frame();
      int stop_stream();
           
      Imlib_Image *get_image();

    protected:
    
      Imlib_Image im;
      
      int device_fd;

      struct video_window vid_window;
      struct video_capability vid_capability;
      struct video_picture vid_picture;
      struct video_capture vid_capture;
      struct video_mbuf vid_mbuf;
      struct video_mmap vid_mmap;
      
      int num_frames;
      int frame;
      
      unsigned char *in_buffer;
      unsigned char *out_buffer;
      unsigned char *mm_buffer;
      
      bool jpeg_frame;
      bool use_mmap;
      
      int bytes_per_pixel;
      
      unsigned long first_frame;
      unsigned long current_frame;
      
      struct jpeg_decompress_struct cinfo;
      struct jpeg_error_mgr jerr;
      
  };

};

#endif
