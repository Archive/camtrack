//    sequencerV4L.cc - video4linux frame sequencer
//
//    Copyright (C) 2005  Adam McCullough <adam.mccullough@mansfield.oxon.org>
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include <iostream>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <stdio.h>


#include "sequencerV4L.h"

#define DEFAULT_DEVICE "/dev/video0"

// jpeg decompression source manager code


static void init_source (j_decompress_ptr cinfo)
{
  buffer_src_ptr src = (buffer_src_ptr) cinfo->src;

  src->start = true;
}

// we're reading from a buffer large enough to contain the whole
// jpeg stream, so no more data can be loaded. If the data's
// incomplete, insert an EOI and settle for what we already got.
static boolean fill_input_buffer (j_decompress_ptr cinfo)
{
  buffer_src_ptr src = (buffer_src_ptr) cinfo->src;

  // You can't have any more!
  src->buffer[0] = (JOCTET) 0xFF;
  src->buffer[1] = (JOCTET) JPEG_EOI;

  src->pub.next_input_byte = src->buffer;
  src->pub.bytes_in_buffer = 2;
  src->start = false;

  return TRUE;
}

static void skip_input_data (j_decompress_ptr cinfo, long num_bytes)
{
  buffer_src_ptr src = (buffer_src_ptr) cinfo->src;
  
  if (num_bytes > 0) {
    src->pub.next_input_byte += (size_t) num_bytes;
    src->pub.bytes_in_buffer -= (size_t) num_bytes;
  }
}

static void term_source (j_decompress_ptr cinfo)
{
  // empty
}


GLOBAL(void)
jpeg_buffer_src (j_decompress_ptr cinfo, JOCTET *inbuffer, size_t size)
{
  buffer_src_ptr src;

  buffer_source_mgr foo;

  if (cinfo->src == NULL) {	// new decompress info struct
    cinfo->src = (struct jpeg_source_mgr *)
      (*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_PERMANENT,
				  sizeof(buffer_source_mgr));
  }
  
  src = (buffer_src_ptr) cinfo->src;
  
  src->pub.init_source = init_source;
  src->pub.fill_input_buffer = fill_input_buffer;
  src->pub.skip_input_data = skip_input_data;
  src->pub.resync_to_restart = jpeg_resync_to_restart; // use libjpeg method
  src->pub.term_source = term_source;

  src->buffer = inbuffer;

  src->pub.bytes_in_buffer = size;
  src->pub.next_input_byte = src->buffer;
}

// end jpeg code




using namespace CamTrack;


SequencerV4L::SequencerV4L()
:
im(NULL),
device_fd(0),
num_frames(1),
frame(0),
in_buffer(NULL),
out_buffer(NULL),
mm_buffer(NULL),
jpeg_frame(false),
use_mmap(false),
bytes_per_pixel(3),
first_frame(0),
current_frame(0){
  
  if((device_fd = open(DEFAULT_DEVICE, O_RDWR)) < 0){
    std::cerr << "couldn't open video device" << std::endl;
    exit(1);
  }
  
  // get capability information from the device
  if (ioctl(device_fd, VIDIOCGCAP, &vid_capability) < 0) {
    std::cerr << "VIDIOGCAP ioctl failed" <<std::endl;
    std::cerr << DEFAULT_DEVICE << " not a video4linux device?" << std::endl;
    close(device_fd);
    exit(1);
  }else{
    std::cout << std::endl << " Video device info" << std::endl;
    std::cout << " - interface               : " << vid_capability.type << " - " << vid_capability.name << std::endl;
    std::cout << " - min capture window size : " << vid_capability.minwidth << "x" << vid_capability.minheight << std::endl;
    std::cout << " - max capture window size : " << vid_capability.maxwidth << "x" << vid_capability.maxheight << std::endl;
  }
  
  if(strcmp(vid_capability.name, "OV519 USB Camera")==0){
    jpeg_frame = true;
    std::cout << "Detected OV519 Camera - frames are jpeg encoded" << std::endl;
  }
  
  // get window information
  if (ioctl(device_fd, VIDIOCGWIN, &vid_window) < 0) {
    std::cerr << "VIDIOCGWIN ioctl failed" <<std::endl;
    close(device_fd);
    exit(1);
  }else{
    std::cout << " Video window info" << std::endl;
    std::cout << " - position                : " << vid_window.x << "," << vid_window.y << std::endl;
    std::cout << " - size                    : " << vid_window.width << "x" << vid_window.height << std::endl;
    std::cout << " - chroma key              : " << vid_window.chromakey << std::endl;
  }
  
  // get picture info
  if (ioctl(device_fd, VIDIOCGPICT, &vid_picture) < 0) {
    std::cerr << "VIDIOCGPICT ioctl failed" <<std::endl;
    close(device_fd);
    exit(1);
  }else{
    std::cout << " Video picture info" << std::endl;
    std::cout << " - brightness              : " << vid_picture.brightness << std::endl;
    std::cout << " - colour                  : " << vid_picture.colour << std::endl;
    std::cout << " - contrast                : " << vid_picture.contrast << std::endl;
    std::cout << " - whiteness               : " << vid_picture.whiteness << std::endl;
    std::cout << " - depth                   : " << vid_picture.depth << std::endl;
    std::cout << " - palette                 : ";
    if(vid_picture.palette == VIDEO_PALETTE_RGB24)
      std::cout << "VIDEO_PALETTE_RGB24" << std::endl;
    else
      std::cout << "non RGB24 palette" << std::endl;
  }
  
  if (vid_capability.type & VID_TYPE_MONOCHROME) {
    std::cerr << "Monochrome cameras are not currently supported" << std::endl;
    close(device_fd);
    exit(1);
  }else if((vid_picture.depth!=24)||(vid_picture.palette!=VIDEO_PALETTE_RGB24)){
    vid_picture.depth=24;
    vid_picture.palette=VIDEO_PALETTE_RGB24;
    
    if(ioctl(device_fd, VIDIOCSPICT, &vid_picture) < 0) {
      std::cerr << "Camera doesn't support RGB24 palette" << std::endl;
      close(device_fd);
      exit(1);
    }
  }
  
  frame_width = vid_window.width;
  frame_height = vid_window.height;
  
  // set up mmap stuff?
  if (vid_capability.type & VID_TYPE_CAPTURE){
    std::cout << "Device supports mmap - configuring" << std::endl;
    use_mmap = true;
    
    if (ioctl(device_fd, VIDIOCGMBUF, &vid_mbuf) < 0) {
      std::cerr << "VIDIOCGMBUF ioctl failed" <<std::endl;
      close(device_fd);
      exit(1);
    }
    
    mm_buffer = (unsigned char*)mmap(0, vid_mbuf.size, PROT_READ, MAP_SHARED,
                     device_fd, 0);
    if(!mm_buffer){
      std::cerr << "mmap failed" << std::endl;
      exit(1);
    }
    
    vid_mmap.format = VIDEO_PALETTE_RGB24;
    vid_mmap.frame = 0;
    vid_mmap.width = frame_width;
    vid_mmap.height = frame_height;
        
  }
  
  if(!use_mmap){
    in_buffer = new unsigned char[frame_width*frame_height*3];
  }
  
  if(jpeg_frame){
    out_buffer = new unsigned char[frame_width*frame_height*3];
    
    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_decompress(&cinfo);
        
  }
  
  
}


SequencerV4L::~SequencerV4L(){
  stop_stream();
  if(!use_mmap){
    delete [] in_buffer;
  }
  if(jpeg_frame){
    jpeg_destroy_decompress(&cinfo);
    delete [] out_buffer;
  }
}
      
int SequencerV4L::reset(){
  
  stop_stream();
  start_stream();
  
  return current_frame = first_frame;
}

int SequencerV4L::start_stream(){
  
  im = imlib_create_image(frame_width, frame_height);


  

  return 1;
}

long SequencerV4L::grab_frame(){
  unsigned char* frame_data;
  
  if(!use_mmap){
    int count = read(device_fd, in_buffer, frame_width*frame_height*3) - 2;
    frame_data = in_buffer;
    
  }else{
    
    if (ioctl(device_fd, VIDIOCMCAPTURE, &vid_mmap) < 0) {
      std::cerr << "VIDIOCMCAPTURE ioctl failed" <<std::endl;
      close(device_fd);
      exit(1);
    }
    if (ioctl(device_fd, VIDIOCSYNC, &vid_mmap.frame) < 0) {
      std::cerr << "VIDIOCSYNC ioctl failed" <<std::endl;
      close(device_fd);
      exit(1);
    }
    
    frame_data = mm_buffer + vid_mbuf.offsets[vid_mmap.frame];
  }
  
  // decompress frame if jpeg encoded
  if(jpeg_frame){
    unsigned short *sizeptr = (unsigned short *)frame_data;
    unsigned int jpeg_size = (unsigned int)(sizeptr[0])<<3;
    
    unsigned char *jpeg_buffer = frame_data+2;
    
    jpeg_buffer_src(&cinfo, jpeg_buffer, jpeg_size);
    jpeg_read_header(&cinfo, true);
    
    jpeg_start_decompress(&cinfo);
    
    unsigned char* pdata;
    while(cinfo.output_scanline < cinfo.output_height){
      pdata = out_buffer+ (cinfo.output_scanline*frame_width*3);
      jpeg_read_scanlines(&cinfo, &pdata, frame_height-cinfo.output_scanline);
    }
    jpeg_finish_decompress(&cinfo);
    
    frame_data = out_buffer;
  }
    
  imlib_context_set_image(im);
  DATA32 *imgdata = imlib_image_get_data();
  int length = frame_width * frame_height;
  unsigned char *inptr = frame_data;
  DATA32 *outptr = imgdata;
  for(unsigned int i=0; i < length; i+=4){

    *outptr = (*inptr<<16) | (*(inptr+1) << 8) | *(inptr+2);
    *(outptr+1) = (*(inptr+3) << 16) | (*(inptr+4) << 8) | *(inptr+5);
    *(outptr+2) = (*(inptr+6) << 16) | (*(inptr+7) << 8) | *(inptr+8);
    *(outptr+3) = (*(inptr+9) << 16) | (*(inptr+10) << 8) | *(inptr+11);
    
    inptr+=12;
    outptr+=4;
  }
  
  imlib_image_put_back_data(imgdata);
  
  ++current_frame;
  
  return current_frame;
}

int SequencerV4L::stop_stream(){
  close(device_fd);
}


Imlib_Image *SequencerV4L::get_image(){
  return &im;
}





