//    event.cc - camtrack event manager
//
//    Copyright (C) 2005  Adam McCullough <adam.mccullough@mansfield.oxon.org>
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include <sys/time.h>
#include <iostream>
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>


#include "event.h"

using namespace CamTrack;


EventManager::EventManager(const unsigned int w,
                           const unsigned int h,
                           const char* fifoname)
:
width(w),height(h),
fifo(0),
x(0.0),dx(0.0),d2x(0.0),
y(0.0),dy(0.0),d2y(0.0),
z(0.0),dz(0.0),d2z(0.0),
tlast(0.0),
dt(0.0),
x_index(0),y_index(0),z_index(0),
button_left(0),button_right(0),
button_left_time(0.0),button_right_time(0.0),
swing_left(0),swing_right(0),
swing_left_time(0.0),swing_right_time(0.0),
move_left(0),move_right(0),
left_time(0.0),right_time(0.0),
move_up(0),move_down(0),
up_time(0.0),down_time(0.0),
state_changed(false)
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  tlast = tv.tv_sec+tv.tv_usec*0.000001;

  for(unsigned int i=0; i < X_SMOOTHING_KERNEL_SIZE; ++i){
    x_history[i] = 0.0;
  }
  for(unsigned int i=0; i < Y_SMOOTHING_KERNEL_SIZE; ++i){
    y_history[i] = 0.0;
  }
  for(unsigned int i=0; i < Z_SMOOTHING_KERNEL_SIZE; ++i){
    z_history[i] = 0.0;
  }
  
  strcpy(fifo_name, fifoname);
  fifo = open(fifo_name, O_WRONLY | O_NONBLOCK);
  
  if (fifo < 0) {
    std::cerr << std::endl << "Failed to open FIFO. Please check that" << std::endl;
    std::cerr << " - the FIFO exists" << std::endl;
    std::cerr << " - its permissions are correct" << std::endl;
    std::cerr << " - it has been opened for reading by X11" << std::endl;
    
    exit(1);
  }
    
}

EventManager::~EventManager(){
  close(fifo);
}

void EventManager::reset_fifo(){
  close(fifo);
  fifo = open(fifo_name, O_WRONLY);
  if (fifo < 0) {
    std::cerr << "Failed to reopen fifo." << std::endl;
    exit(1);
  }
  
}

void EventManager::update(TrackingWindow &win){
  
  // state change variables
  state_changed = false;
  int xdisp = 0;
  int ydisp = 0;
    
  // set time interval since last update
  struct timeval tv;
  gettimeofday(&tv, NULL);
  double tnow = tv.tv_sec+tv.tv_usec*0.000001;
  dt = tnow - tlast;
  tlast = tnow;
  
  // update timers
  if(swing_left > 0){
    swing_left_time += dt;
    if(swing_left_time > SWING_TIMEOUT){
      swing_left = 0;
    }
  }
  if(button_left > 0){
    button_left_time += dt;
    if(button_left_time > BUTTON_TIME){
      button_left = 0;
      state_changed = true;
    }
  }

  if(swing_right > 0){
    swing_right_time += dt;
    if(swing_right_time > SWING_TIMEOUT){
      swing_right = 0;
    }
  }
  if(button_right > 0){
    button_right_time += dt;
    if(button_right_time > BUTTON_TIME){
      button_right = 0;
      state_changed = true;
    }
  }

  // calculate the position, velocity and acceleration
  // of the tracking window in x and y directions
  float xn = win.get_centre_x();
  
  x_history[x_index] = xn;
  ++x_index;
  if(x_index>=X_SMOOTHING_KERNEL_SIZE) x_index = 0;
  unsigned int n = 0;
  xn = 0.0;
  for(unsigned int i=0; i < X_SMOOTHING_KERNEL_SIZE; ++i){
    if(x_history[i]>0.0){
      xn += x_history[i];
      ++n;
    }
  }
  xn /= n;
  float dxn = (xn - x);

  // detect button triggers
  //std::cerr << "dx, dxn : " << dx << "," << dxn << std::endl;
  // detect left->right velocity change
  if((dx>0.0)&&(dxn<0.0)&&((dx-dxn)>5.0)){
    if(swing_right > 0){
      button_right = 1;
      swing_right = 0;
      state_changed = true;
      button_right_time = 0.0;
      std::cerr << "right click!" << std::endl;
    }else{
      swing_left = 1;
      swing_left_time = 0.0;
      std::cerr << "swing left detected" << std::endl;
    }
  }
  // detect right->left velocity change
  if((dx<-0.0)&&(dxn>0.0)&&((dxn-dx)>5.0)){
    if(swing_left > 0){
      button_left = 1;
      swing_left = 0;
      state_changed = true;
      button_left_time = 0.0;
      std::cerr << "left click!" << std::endl;
    }else{
      swing_right = 1;
      swing_right_time = 0.0;
      std::cerr << "swing right detected" << std::endl;
    }
  }
  
  d2x = (dxn - dx);
  dx = dxn;
  x = xn;
  
  
  float yn = win.get_centre_y();
  y_history[y_index] = yn;
  ++y_index;
  if(y_index>=Y_SMOOTHING_KERNEL_SIZE) y_index = 0;
  n = 0;
  yn = 0.0;
  for(unsigned int i=0; i < Y_SMOOTHING_KERNEL_SIZE; ++i){
    if(y_history[i]>0.0){
      yn += y_history[i];
      ++n;
    }
  }
  yn /= n;
  float dyn = (yn - y);
  d2y = (dyn - dy);
  dy = dyn;
  y = yn;
  
  // the z direction derives from the size of the tracking window,
  // and shows a bit more `jitter' due to interactions between
  // artificial lighting and the camera sampling and other fluctuating
  // effects. As a result, it needs more temporal smoothing to filter the
  // high frequency variations.
  
  float zn = 100000.0/win.get_width();
  
  z_history[z_index] = zn;
  ++z_index;
  if(z_index>=Z_SMOOTHING_KERNEL_SIZE) z_index = 0;
  n = 0;
  zn = 0.0;
  for(unsigned int i=0; i < Z_SMOOTHING_KERNEL_SIZE; ++i){
    if(z_history[i]>0.0){
      zn += z_history[i];
      ++n;
    }
  }
  zn /= n;

  float dzn = (zn - z);
  d2z = (dzn - dz);
  dz = dzn;
  z = zn;
  // so now we are tracking x, y, and z - position, velocity and acceleration
  
  // detect proximity to edges of FOV, trigger continuous scrolling
  // TODO - sort out hardcoded values
  
  if(x < width*7/16){ // move right
    right_time += dt;
    if(move_right > 0){
      xdisp = 0.05 * right_time * (x - (width*7/16));
      if(xdisp > 7) xdisp = 7;
      state_changed = true;
    }else if(right_time > MOVE_TIMEOUT){
      move_right = 1;
      right_time = 0.0;
    }else{
    }
  }else{
    move_right = 0;
    right_time = 0.0;
  }
  
  if(x > width*9/16){ // move left
    left_time += dt;
    if(move_left > 0){
      xdisp = 0.05 * left_time * (x -(width*9/16));
      if(xdisp < -7) xdisp = -7;
      state_changed = true;
    }else if(left_time > MOVE_TIMEOUT){
      move_left = 1;
      left_time = 0.0;
    }else{
    }
  }else{
    move_left = 0;
    left_time = 0.0;
  }

  if(y > height*9/16){ // move down
    down_time += dt;
    if(move_down > 0){
      ydisp = 0.05 * down_time * (y -(height*9/16));
      if(ydisp < -7) ydisp = -7;
      state_changed = true;
    }else if(down_time > MOVE_TIMEOUT){
      move_down = 1;
      down_time = 0.0;
    }else{
    }
  }else{
    move_down = 0;
    down_time = 0.0;
  }

  if(y < height*7/16){ // move up
    up_time += dt;
    if(move_up > 0){
      ydisp = 0.05 * up_time * (y -(height*7/16));
      if(ydisp > 7) ydisp = 7;
      state_changed = true;
    }else if(up_time > MOVE_TIMEOUT){
      move_up = 1;
      up_time = 0.0;
    }else{
    }
  }else{
    move_up = 0;
    up_time = 0.0;
  }


  // compose the output packet to send to the fifo
  // in Microsoft serial mouse format
  if(state_changed){
    unsigned char out[3];
    unsigned char uctmp;
    char ctmp;
    
    out[0] = 0x40;
    if(button_left) out[0] |= 0x20;
    if(button_right) out[0] |= 0x10;
    
    ctmp = static_cast<char>(ydisp);
    uctmp = static_cast<unsigned char>(ctmp);
    out[2] = uctmp & 0x3F;
    uctmp >>= 4;
    out[0] |= (uctmp & 0x0C);
    
    ctmp = -1 * static_cast<char>(xdisp);
    uctmp = static_cast<unsigned char>(ctmp);
    out[1] = uctmp & 0x3F;
    uctmp >>= 6;
    out[0] |= (uctmp & 0x03);
    
    int r = write(fifo, out, 3);
    //std::cerr << "Wrote " << r << " bytes to fifo." << std::endl;
  }
  
}









