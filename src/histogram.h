// Copyright (C) 2005 Adam McCullough <adam.mccullough@mansfield.oxon.org>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef CT_HISTOGRAM
#define CT_HISTOGRAM

#define HISTOGRAM_VERSION "0.1"

namespace CamTrack{


  /*!
   *  \brief Represents a histogram of arbitrary dimensionality
   *
   *         The histogram object is initialized with the valid range of values
   *         for each component, the number of components (ie. the number of 
   *         dimensions of the histogram) and the required resolution (ie. the
   *         number of bins covering the specified range in each dimension).
   */
  class Histogram{
    
    public:
      Histogram(int minval, int maxval, int resolution);
      Histogram(std::istream& input);
      Histogram(std::istream& input, int minval, int maxval, int resolution);
      ~Histogram();
      
      unsigned int count(int point);
      unsigned int count_stream(std::istream& input);
      unsigned int get_value_by_point(int point){
        return table[binlookup[point]];
      }
      unsigned int set_bin_value(unsigned int bin, unsigned int val);
      unsigned int get_bin_value(unsigned int bin);
      
      unsigned int write(std::ostream& output);
      
      int get_minval();
      int get_maxval();
      int get_resolution();
          
    private:
      int maxval, minval;
      int resolution;
      float bin_size;
      float inv_bin_size;
      
      unsigned int *table;
      
      unsigned int *binlookup;
      
      unsigned int get_value_by_index(unsigned int index);

      unsigned int *get_bin_by_index(unsigned int index);
      
      void init(int minval, int maxval, int resolution);
      
  };

};

#endif
