// readimage.cc - retrieve pixel data from images
//
// Copyright (C) 2005 Adam McCullough <adam.mccullough@mansfield.oxon.org>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <iostream>
#include <fstream>
#include <getopt.h>
#include <string.h>
#include <X11/Xlib.h>
#include <Imlib2.h>
#include <math.h>

int gopt_mask = 0;
int gopt_unmask = 0;

static struct option long_options[]={
  {"mask", no_argument, &gopt_mask, 1},           // mask pixel data
  {"unmask", no_argument, &gopt_mask, 1},         // mask pixel data inverting mask data
  {"cspace", required_argument, NULL, 0},         // specify colourspace
  {"help", no_argument, NULL, 0},                 // print help info
  {NULL, 0,0,0}
};

int get_options(int argc, char* argv[]){
  int opt_index = 0;
  
  while(getopt_long(argc, argv, "", long_options, &opt_index)>=0){
    switch(opt_index){
      case 1: // --unmask
        gopt_unmask = 1;
        break;
      case 2: // --cspace
//        if(strcmp(optarg, "nrgb")==0){
//          gopt_nrgb = 1;
//          gopt_hs = 0;
//        }
//        else if(strcmp(optarg, "hs")==0){
//          gopt_hs = 1;
//          gopt_nrgb = 0;
//        }
//        else{
//          std::cerr << "Specified cspace is invalid." << std::endl;
//          exit(0);
//        }
        break;
      case 3: // --help
        std::cout
        << "Usage: readimage [options] filename [...]" << std::endl
        << "Read pixel data from images to std output." << std::endl
        << "Options:" << std::endl
        << "  --help                  Display usage information" << std::endl

        << "  --mask                  Use mask to restrict output pixel areas" << std::endl
        << "                          For each filename given, there must be a corresponding" << std::endl
        << "                          mask image, identified by a \"_mask\" at the end of" << std::endl
        << "                          the filename and before any extension. For example," << std::endl
        << "                          the mask for an image named \"foo.ppm\" would be" << std::endl
        << "                          \"foo_mask.ppm\"." << std::endl
        
        << "  --unmask                Output only unmasked pixels (ie. the pixels ommitted" << std::endl
        << "                          if \"--mask\" is used" << std::endl
        
//        << "  --cspace <colourspace>  Specify the colourspace of the output data" << std::endl
//        << "                          <colourspace> must be either \"nrgb\" or \"hs\"," << std::endl
//        << "                          producing two component output in normalized RGB" << std::endl
//        << "                          and the hue and saturation components of HSV" << std::endl
//        << "                          representation respectively." << std::endl << std::endl
        << "For more information, please see the README file distributed with this software." << std::endl;
        exit(0);
        break;
      default:
        if(gopt_mask == 0){
          std::cerr << "Please try \"readimage --help\" for usage details." << std::endl;
          exit(1);
        }
        break;
    }
  }
  return optind;
}


int main(int argc, char* argv[]){
  int fname_index = -1;
  Imlib_Image image;
  Imlib_Image mask_image;
    
  // parse and respond to command line
  fname_index = get_options(argc, argv);
  
  if((fname_index >= argc)||(fname_index < 0)){
    std::cerr << "No filename given." << std::endl;
    exit(0);
  }
  
  //cycle through all images given on command line
  for( ; fname_index<argc; ++fname_index){
  
    // open image
    image = imlib_load_image(argv[fname_index]);
    // exit if load failed
    if(!image){
      std::cerr << "Image load failed on file " << argv[fname_index] << std::endl;
      exit(1);
    }
  
    // check image dimensions
    imlib_context_set_image(image);
    int w = imlib_image_get_width();
    int h = imlib_image_get_height();
    
    // open and read mask if necessary
    if(gopt_mask){
      // generate mask name
      char maskname[CT_FILENAME_LENGTH] = "";
      char *ext = rindex(argv[fname_index], '.');
      char *end = index(argv[fname_index], '\0');
      if( (ext==NULL)||(ext>end) )
        ext = end;
      int l = ext-argv[fname_index]; 
      strncat(maskname, argv[fname_index], l);
      strcat(maskname, "_mask");
      strcat(maskname, ext);
      
      // load mask
      mask_image = imlib_load_image(maskname);
      // exit if load failed
      if(!mask_image){
        std::cerr << "Mask Image load failed on file " << maskname << std::endl;
        exit(1);
      }
    
      // check mask dimensions match image dimensions
      imlib_context_set_image(mask_image);
      if( (w != imlib_image_get_width()) || (h != imlib_image_get_height()) ){
        std::cerr << "Image and mask dimensions differ." << std::endl;
        exit(1);
      }
    }
  
    // cycle through all pixels in image
    Imlib_Color cmask;
    Imlib_Color cpixel;
    float hue,sat,val;
    int a;
  
    for(int y=0; y < h; ++y){
      for(int x=0; x < w; ++x){
        bool masked = false;
        
        if(gopt_mask){
          imlib_context_set_image(mask_image);
          imlib_image_query_pixel(x, y, &cmask);
                  
          if( (cmask.red > 0) || (cmask.green > 0) || (cmask.blue > 0) )
            masked = true;
          
          if(gopt_unmask) masked = !masked;
        }
        
        // output pixel data
        if(!masked){
          // produce output dependent on selected colourspace
//          if(gopt_nrgb == 1){
//            imlib_context_set_image(image);
//            imlib_image_query_pixel(x, y, &cpixel);
//            int nr, ng, sum;
//            sum = cpixel.red + cpixel.green +cpixel.blue;
//            if(sum > 0){
//              nr = (cpixel.red * 255) / sum;
//              ng = (cpixel.green * 255) / sum;
//            }else{
//              nr = 0;
//              ng = 0;
//            }
            
//            std::cout << nr << "," << ng << std::endl;
            
//          }else if(gopt_hs == 1){
            float h,s,v;
            int a;
            
            imlib_context_set_image(image);
            imlib_image_query_pixel_hsva(x, y, &h, &s, &v, &a);
                        
            if(isnan(h)) h = -1.0;
            else{
              h *= 2.55;
              std::cout << static_cast<int>(round(h)) << std::endl;
            }
        }
      }
    }
    
    imlib_context_set_image(image);
    imlib_free_image_and_decache();
    if(gopt_mask){
      imlib_context_set_image(mask_image);
      imlib_free_image_and_decache();
    }
    
  }
  return 0;
}

