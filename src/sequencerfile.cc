//    sequencerfile.cc - frame sequencer for series of frames stored as files
//
//    Copyright (C) 2005  Adam McCullough <adam.mccullough@mansfield.oxon.org>
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "sequencerfile.h"

using namespace CamTrack;

SequencerFile::SequencerFile(const char* p_filename){
  strcpy(filename, p_filename);
}

SequencerFile::~SequencerFile(){
  if(imlib_context_get_image() != NULL){
    imlib_free_image_and_decache();
  }

}

/**
 * initialise the stream.
 * returns the number of available frames, or -1 if no frames are found
 */
int SequencerFile::start_stream(){
  
  // Determine the filename arrangement and search to find the
  // number of frames in the supplied series, and the numbers of the
  // first and last frames in the series
  std::cout << "Initialising stream..." << fileprefix << std::endl;
  
  char* file = rindex(filename, '/') + 1;
  char filedir[128] = "";
  
  strncpy(filedir, filename, file-filename);
  
  sscanf(file, "%[a-zA-Z]%u%s", fileprefix, &current_frame, filesuffix );
  
  strcat(filedir, fileprefix);
  strcpy(fileprefix, filedir);
  
  numdigits = strlen(filename) - ( strlen(fileprefix) + strlen(filesuffix) );
  
  std::cout << " -Prefix                   : " << fileprefix << std::endl;
  std::cout << " -Suffix                   : " << filesuffix << std::endl;
  std::cout << " -Current frame            : " << current_frame << std::endl;
  
  unsigned long num_frames;
  FILE* rstream;
  char shellout[256] = "";
  char pattern[128] = "";

  sprintf(shellout, "echo `ls %s*%s | wc -w`", fileprefix, filesuffix);
  
  if( (rstream = popen(shellout, "r")) == (FILE*)NULL ){
    std::cout << "Frame count failed" << std::endl;
    return -1;
  }else{
    fscanf(rstream, "%u", &num_frames);
    std::cout << " -Number of frames         : " << num_frames << std::endl;
  }
  pclose(rstream);
  
  sprintf(shellout, "echo `ls %s*%s | sort | sed -n 1p`", fileprefix, filesuffix);
  
  if( (rstream = popen(shellout, "r")) == (FILE*)NULL ){
    std::cout << "Search for first frame number failed" << std::endl;
    return -1;
  }else{
    sprintf(pattern, "%s%s%s", fileprefix, "%u", filesuffix);
    fscanf(rstream, pattern, &first_frame);
    std::cout << " -First frame              : " << first_frame << std::endl;
  }
  pclose(rstream);
 
  sprintf(shellout, "echo `ls %s*%s | sort | sed -n %up`", fileprefix, filesuffix, num_frames);
  
  if( (rstream = popen(shellout, "r")) == (FILE*)NULL ){
    std::cout << "Search for last frame number failed" << std::endl;
    return -1;
  }else{
    sprintf(pattern, "%s%s%s", fileprefix, "%u", filesuffix);
    fscanf(rstream, pattern, &last_frame);
    std::cout << " -Last frame               : " << last_frame << std::endl;
  }
  pclose(rstream);
  
  // Find the width and height of the frames in the series
  // by opening the specified frame
  
  im = imlib_load_image(get_current_filename());
  imlib_context_set_image(im);
  frame_width = imlib_image_get_width();
  frame_height = imlib_image_get_height();
  
  std::cout << " -Width                    : " << frame_width << std::endl;
  std::cout << " -Height                   : " << frame_height << std::endl;
  
  return num_frames;
}

int SequencerFile::reset(){
  current_frame = first_frame;
}

long SequencerFile::grab_frame(){

  if(imlib_context_get_image() != NULL){
    imlib_free_image_and_decache();
  }

  if(current_frame > last_frame)
    return -1;
  else{
    im = imlib_load_image(get_current_filename());
  //  std::cout << "Loading image : " << current_filename << std::endl;
    ++current_frame;
    
    return current_frame;
  }
}

int SequencerFile::stop_stream(){
  if(imlib_context_get_image() != NULL){
    imlib_free_image_and_decache();
  }
}

const char* SequencerFile::get_current_filename(){
  sprintf(current_filename, "%s%.*u%s", fileprefix, numdigits, current_frame, filesuffix);
  return current_filename;
}

Imlib_Image *SequencerFile::get_image(){
  return &im;
}
