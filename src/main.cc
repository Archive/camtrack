// main.cc - camera-based face tracker
//
// Copyright (C) 2005 Adam McCullough <adam.mccullough@mansfield.oxon.org>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <iostream>
#include <fstream>
#include <getopt.h>
#include <string.h>
#include <X11/Xlib.h>
#include <Imlib2.h>
#include <unistd.h>

#include "sequencer.h"
#include "sequencerfile.h"
#include "sequencer1394.h"
#include "sequencerV4L.h"
#include "framedisplay.h"
#include "histogram.h"
#include "probmap.h"
#include "mstracker.h"
#include "window.h"
#include "event.h"

using namespace CamTrack;


int gopt_use_file = 0;
int gopt_use_1394 = 0;
int gopt_use_v4l = 0;
char g_filename[128] = "";
char g_fifoname[128] = "/dev/camtrack";
u_int64_t g_1344_guid = 0;

static struct option long_options[]={
  {"file", 1, &gopt_use_file, 1},
  {"1394", 0, &gopt_use_1394, 1},
  {"v4l", 0, &gopt_use_v4l, 1},
  {"help", 0, NULL, 0},
  {NULL, 0,0,0}
};

int get_options(int argc, char* argv[]);
void build_lookup_histogram(Histogram **hist);

int main(int argc, char* argv[]){

  get_options(argc, argv);
  Display* disp;

  Sequencer *seq;
  Histogram *hist;

  disp = XOpenDisplay(NULL);
  
  // create the sequencer
  std::cerr << "Constructing sequencer     : ";
  if(gopt_use_file){
    seq = new SequencerFile(g_filename);
  }else if(gopt_use_1394){
    seq = new Sequencer1394(g_1344_guid);
  }else if(gopt_use_v4l){
    seq = new SequencerV4L();
  }else{
    std::cout << "No sequencer type selected" << std::endl;
    exit(0);
  }
  std::cerr << "OK" << std::endl;
  
  // prepare the lookup histogram
  build_lookup_histogram(&hist);
  
  // initialise the video stream
  std::cerr << "Starting stream            : ";
  if(seq->start_stream() < 0){
    std::cerr << "No stream available from selected source" << std::endl;
    exit(1);
  }
  std::cerr << "OK" << std::endl;

  // create the display window
  std::cerr << "Constructing monitor       : ";
  FrameDisplay frame(seq->get_width(), seq->get_height());
  std::cerr << "OK" << std::endl;
  
  // create the probability map
  std::cerr << "Building face map          : ";
  ProbMap map(seq->get_width(), seq->get_height());
  std::cerr << "OK" << std::endl;
  
  // create the tracking window & tracker
  std::cerr << "Constructing tracker       : ";
  TrackingWindow win(0,0,seq->get_width(), seq->get_height());
  MeanShiftTracker tracker(map, win);
  std::cerr << "OK" << std::endl;
  
  Imlib_Image foo = imlib_create_image(seq->get_width(),seq->get_height());
  
  std::cerr << "Constructing event manager : ";
  EventManager event(seq->get_width(), seq->get_height(), g_fifoname);
  std::cerr << "OK" << std::endl;
  
  // while the stream continues to provide frames...
  while(seq->grab_frame() >= 0){
  
    // build the probability map
    map.scan(seq->get_image(), hist, win);
    // apply "close" morphology to the image ()
    //std::cerr << "Applying morphology" << std::endl;
    map.close();
    
    // run the tracker
    //std::cerr << "Running Tracker" << std::endl;
    tracker.track();
    
    if(tracker.is_tracking()){
      map.set_eye_region(win);
      
      //std::cerr << "Updating event manager" << std::endl;
      event.update(win);
      
    }
    // display the prob map
    //std::cerr << "Updating display" << std::endl;
    
    
    
    imlib_context_set_image(foo);
    
    imlib_context_set_color(255,255,255,255);
    imlib_image_fill_rectangle(0,0,seq->get_width(),seq->get_height());
    
    int iw = seq->get_width();
    int ih = seq->get_height();
       
    imlib_context_set_color(0,0,0,255);
    
    for(int y=0; y < ih; ++y){
      for(int x=0; x < iw; ++x){
        if(map.get_pixel(x,y)==0){
          imlib_image_draw_line(x,y,x,y,0);
        }
      }
    }
    
    if(tracker.is_tracking()){    
      imlib_context_set_color(0,0,255,255);
      imlib_image_draw_rectangle(win.get_left(), win.get_top(), win.get_width(), win.get_height());
      
      //imlib_context_set_color(255,0,0,255);
      //imlib_image_draw_rectangle(map.eyeregion_l, map.eyeregion_t, map.eyeregion_r-map.eyeregion_l, win.get_height()/3);
    }

    frame.draw_frame(&foo);
 
    /*
    
    imlib_context_set_image(*(seq->get_image()));
    
    //imlib_context_set_color(0,0,255,255);
    //imlib_image_draw_rectangle(win.left(), win.top(), win.width(), win.height());
        
    // display the captured frame
    frame.draw_frame(seq->get_image());
    */
  }

  imlib_context_set_image(foo);
  imlib_free_image();
  
  seq->stop_stream();
  
  delete hist;
  delete seq;

  return 0;
}


int get_options(int argc, char* argv[]){
  
  int opt_index = -1;
  while(getopt_long(argc, argv, "", long_options, &opt_index)>=0){
    switch(opt_index){
      case 0:
        strcpy(g_filename, optarg);
        std::cerr << "Streaming from file        : " << g_filename << std::endl;
        break;
      case 1:
        std::cerr << "Streaming from 1394 source" << std::endl;
        break;
      case 2:
        std::cerr << "Streaming from video4linux source" << std::endl;
        break;
      case 3: // --help
        std::cout
        << "Usage: camtrack [options]" << std::endl
        << "Options:" << std::endl
        << "  --help                  Display usage information" << std::endl

        << "  --file <IMAGEFILE>      Use a saved image series as input" << std::endl
        << "                          IMAGEFILE should be the first frame of the series," << std::endl
        << "                          which should have sequentially numbered filenames" << std::endl
        << "                          such as \"foo_bar001.ppm, foo_bar002.ppm, etc\"" << std::endl
        
        << "  --1394                  Use ieee-1394 digital camera for input" << std::endl
        << "                          Assumes the device is on /dev/video0" << std::endl
        
        << "  --v4l                   Use video4linux for input" << std::endl
        << "                          Assumes the device is on /dev/video0" << std::endl

        << "For more information, please see the README file distributed with this software." << std::endl;
        exit(0);
        break;
      dafault:
        break;
    }
    
  }
  
  return 0;
}

void build_lookup_histogram(Histogram **hist){
  
  // load face and non-face histograms
  std::filebuf fb;
  
  char histfile[256] = "";
  sprintf(histfile, "/home/%s/.camtrack/face_histogram", getlogin());
  
  //#ifdef DEBUG
  std::cerr << "Loading face histogram     : " << histfile << std::endl;
  //#endif
  fb.open(histfile, std::ios::in);
  std::istream input(&fb);
  Histogram hface(input);
  fb.close();
  input.clear();
  
  sprintf(histfile, "/home/%s/.camtrack/non-face_histogram", getlogin());

  //#ifdef DEBUG
  std::cerr << "Loading non-face histogram : " << histfile << std::endl;
  //#endif
  fb.open(histfile, std::ios::in);  
  Histogram hnon_face(input);
  fb.close();
  
  // use bayes theorem with maximum likelihood (ML) estimation to build lookup histogram
  int res = hface.get_resolution();
    
  //#ifdef DEBUG
  std::cerr << "Building lookup histogram  : ";
  //#endif
  *hist = new Histogram(hface.get_minval(), hface.get_maxval(), res);
  
  unsigned int facetotal = 0;
  unsigned int nonfacetotal = 0;
  for(unsigned int i=0; i < res; ++i){
    facetotal += hface.get_bin_value(i);
    nonfacetotal += hnon_face.get_bin_value(i);
  }
  
  for(unsigned int i=0; i < res; ++i){
    float faceprob = static_cast<float>(hface.get_bin_value(i)) / static_cast<float>(facetotal);
    float nonfaceprob = static_cast<float>(hnon_face.get_bin_value(i)) / static_cast<float>(nonfacetotal);
    
    if(faceprob >= nonfaceprob)
      (*hist)->set_bin_value(i, 1);
    else
      (*hist)->set_bin_value(i, 0);
    }
  //fdef DEBUG
  std::cerr << "OK" << std::endl;
  //#endif
}




