//    mstracker.cc - mean shift tracker
//
//    Copyright (C) 2005  Adam McCullough <adam.mccullough@mansfield.oxon.org>
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include <iostream>
#include <math.h>

#include <config.h>

#include "float_cast.h"
#include "mstracker.h"

using namespace CamTrack;

MeanShiftTracker::MeanShiftTracker(ProbMap& rmap, TrackingWindow& rwin)
: 
map(rmap),
win(rwin),
tracking(false),
bounding(false),
framenumber(0),
aspect(WINDOW_ASPECT),
size_factor(WIN_SIZE_FACTOR),
density_threshold(0.0){
  
}

MeanShiftTracker::~MeanShiftTracker(){

}   
  
void MeanShiftTracker::reset(){
  win.set(0,0,map.get_width(), map.get_height());
  tracking = false;
  bounding = false;
}

void MeanShiftTracker::track(){
  int cx, cy, width, height;
  cx = cy = 0;
  
  tracking = true;
  bool centre_converged = false;
  bool window_converged = false;
  bool track_complete = false;
  
  unsigned int cycle = 0;
  unsigned long moment00, moment10, moment01, m00sqrt;
  float density;
  unsigned int val;
  bounding = false;
      
  
  //std::cout << "frame    : " << framenumber << std::endl;

  while(!track_complete){
    moment00 = moment01 = moment10 = m00sqrt = 0;
    
    //std::cout << "cycle    : " << cycle << std::endl;
    
    for(unsigned int y = win.get_top(); y < win.get_bottom(); ++y){
      for(unsigned int x = win.get_left(); x < win.get_right(); ++x){
        if(map.get_pixel(x,y)>0){
          ++moment00;
          moment10 += x;
          moment01 += y;
        }
      }
    }
    //std::cout << "moment00   : " << moment00 << std::endl;
    
    m00sqrt = lrintf(sqrt(moment00));
  
    width = lrintf(m00sqrt*size_factor) | 0x1;
    height = lrintf(width*aspect) | 0x1;

    //std::cout << "old win  : " << win.get_width() << "x" << win.get_height() << std::endl;
    //std::cout << "new win  : " << width << "x" << height << std::endl;
  
    int dw = (width - win.get_width()) >> 1;
    int dh = (height - win.get_height()) >> 1;
    //std::cout << "dw, dh   : " << dw << "," << dh << std::endl;
    
    if( (win.get_left()-dw < 0) || (win.get_right()+dw > map.get_width()) ||
        (win.get_top()-dh < 0) || (win.get_bottom()+dh > map.get_height()) ){
        track_complete = true;
        bounding = true;
        tracking = false;
    }else{
      
      if(((dw*dw)+(dh*dh))<CONVERGENCE_DIST)
        window_converged = true;
      
      if(width > MIN_WINDOW_SIZE){
        win.set_width(width);
        win.set_height(height);
      }else{
        track_complete = true;
        tracking = false;
      }
    }

    if(!centre_converged){
      cx = moment10/moment00;
      cy = moment01/moment00;
      
    //std::cout << "old c    : " << win.get_centre_x() << "," << win.get_centre_y() << std::endl;
    //std::cout << "new c    : " << cx << "," << cy << std::endl;

      int dx = cx - win.get_centre_x();
      int dy = cy - win.get_centre_y();
    //std::cout << "dx, dy   : " << dx << "," << dy << std::endl;
    
      if( (win.get_left()+dx < 0) || (win.get_right()+dx > map.get_width()) ||
        (win.get_top()+dy < 0) || (win.get_bottom()+dy > map.get_height()) ){
        track_complete = true;
        tracking = false;
        bounding = true;
      }else{
        if(win.sq_distance_to_centre(cx, cy) < CONVERGENCE_DIST)
          centre_converged = true;
      
        win.set_centre(cx, cy);
        window_converged = false;
      }
    }
        
    if(window_converged && centre_converged){
      track_complete = true;
      tracking = true;
    }
    ++cycle;
  }

  if(!tracking) reset();

  density = static_cast<float>(moment00) / static_cast<float>(win.get_width()*win.get_height());
  if(framenumber == 0) density_threshold = density*0.6;
  else if(density < density_threshold) reset();

  ++framenumber;
}



