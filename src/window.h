//    window.h - tracking window
//
//    Copyright (C) 2005  Adam McCullough <adam.mccullough@mansfield.oxon.org>
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef CT_WINDOW
#define CT_WINDOW

#include "config.h"

namespace CamTrack{

  class TrackingWindow{

    public:
      TrackingWindow():l(0),t(0),r(0),b(0){}
      TrackingWindow(int li, int ti, int wi, int hi):l(li),t(ti),r(li+wi),b(ti+hi){}
      
      ~TrackingWindow(){}
      
      int get_left(){return l;}
      int get_top(){return t;}
      int get_right(){return r;}
      int get_bottom(){return b;}
      int get_width(){return r-l;}
      int get_height(){return b-t;}
      
      int get_centre_x(){return (l+r)>>1;}
      int get_centre_y(){return (t+b)>>1;}
      
      
      void set(int left, int top, int width, int height){
        l = left;
        t = top;
        r = left + width;
        b = top + height;
      }
      
      void set_width(unsigned int width){
        l = (l+r-width)>>1;
        r = l+width;
      }
      void set_height(unsigned int height){
        t = (t+b-height)>>1;
        b = t+height;
      }
      void set_centre(unsigned int x, unsigned int y){
        int dx = x - get_centre_x();
        int dy = y - get_centre_y();
        l+=dx; r+=dx;
        t+=dy; b+=dy;
      }
      
      unsigned int sq_distance_to_centre(unsigned int x, unsigned int y){
        int dx = (get_centre_x())-x;
        int dy = (get_centre_y())-y;
        return ((dx*dx)+(dy*dy));
      }
      
    protected:
      int l,t,r,b;
      
  };

};

#endif
