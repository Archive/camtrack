//    sequencer1394.cc - ieee-1394 (firewire) frame sequencer
//
//    Copyright (C) 2005  Adam McCullough <adam.mccullough@mansfield.oxon.org>
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include <iostream>

#include "sequencer1394.h"


using namespace CamTrack;


Sequencer1394::Sequencer1394(const u_int64_t guid)
:
im(NULL),
handle(NULL),
first_frame(0),
last_frame(0),
current_frame(0){
  
  struct raw1394_portinfo ports[MAX_PORTS];
  nodeid_t * camera_nodes = NULL;
  int num_ports = 0;
  int num_cameras = 0;
  int reset = 0;
  int found = 0;
  int port = 0;
  
  handle = raw1394_new_handle();

  if (handle==NULL)
  {
    fprintf( stderr, "Unable to aquire a raw1394 handle\n\n"
             "Please check \n"
	     "  - if the kernel modules `ieee1394',`raw1394' and `ohci1394' are loaded \n"
	     "  - if you have read/write access to /dev/raw1394\n\n");
    exit(1);
  }
	/* get the number of ports (cards) */
  num_ports = raw1394_get_port_info(handle, ports, num_ports);
  raw1394_destroy_handle(handle);
  handle = NULL;
  
  for (reset=0; reset < MAX_RESETS && found == 0; ++reset)
  {
    /* look across all ports for cameras */
    for (port = 0; port < num_ports && found == 0; ++port)
    {
      if (handle != NULL)
        dc1394_destroy_handle(handle);
      handle = dc1394_create_handle(port);
      if (handle == NULL)
      {
        std::cerr << "Unable to acquire a raw1394 handle for port " << port << std::endl;
        exit(1);
      }
      num_cameras = 0;
      camera_nodes = dc1394_get_camera_nodes(handle, &num_cameras, 0);
      
      if (num_cameras > 0)
      {
        if (guid == 0)
        {
          dc1394_camerainfo info;
          /* use the first camera found */
          camera.node = camera_nodes[0];
          if (dc1394_get_camera_info(handle, camera_nodes[0], &info) == DC1394_SUCCESS)
            //dc1394_print_camera_info(&info);
          found = 1;
        }
        else
        {
          /* attempt to locate camera by guid */
          for (int k = 0; k < num_cameras && found == 0; ++k)
          {
            dc1394_camerainfo info;
            if (dc1394_get_camera_info(handle, camera_nodes[k], &info) == DC1394_SUCCESS)
            {
              if (info.euid_64 == guid)
              {
                dc1394_print_camera_info(&info);
                camera.node = camera_nodes[k];
                found = 1;
              }
            }
          }
        }
        if (found == 1)
        {
          /* camera can not be root--highest order node */
          if (camera.node == raw1394_get_nodecount(handle)-1)
          {
            /* reset and retry if root */
            raw1394_reset_bus(handle);
            sleep(2);
            found = 0;
          }
        }
        dc1394_free_camera_nodes(camera_nodes);
      } /* cameras >0 */
    } /* next port */
  } /* next reset retry */
  
  if (found == 0 && guid != 0)
  {
    std::cerr << "Unable to locate camera node by guid" << std::endl;
    exit(1);
  }
  else if (num_cameras == 0)
  {
    std::cerr << "no cameras found" << std::endl;
    dc1394_destroy_handle(handle);
    exit(1);
  }
  if (reset == MAX_RESETS)
  {
    std::cerr << "failed to not make camera root node" << std::endl;
    dc1394_destroy_handle(handle);
    exit(1);
  }  
  
}


Sequencer1394::~Sequencer1394(){
  stop_stream();
}
      
int Sequencer1394::reset(){
  
  stop_stream();
  start_stream();
  
  return current_frame = first_frame;
}

int Sequencer1394::start_stream(){
  unsigned int channel;
  unsigned int speed;
  if ( dc1394_get_iso_channel_and_speed( handle, 
				  camera.node,
				  &channel, 
				  &speed ) !=DC1394_SUCCESS )  
  {
     fprintf( stderr, "Unable to get the iso channel number\n" );
     stop_stream();
     return 1;
  }

  if (dc1394_dma_setup_capture(handle, camera.node,
                           channel,
                           FORMAT_VGA_NONCOMPRESSED,
                           MODE_640x480_RGB,
                           SPEED_400,
                           FRAMERATE_15,
                           8, // buffers
                           1, // drop frames
                           DMA_DEVICE_FILE,
                           &camera)!=DC1394_SUCCESS) 
  {
    fprintf( stderr,"unable to setup camera-\n"
             "check line %d of %s to make sure\n"
             "that the video mode,framerate and format are\n"
             "supported by your camera\n",
             __LINE__,__FILE__);
    stop_stream();
    exit(1);
  }
  
  frame_width = camera.frame_width;
  frame_height = camera.frame_height;
  
  im = imlib_create_image(frame_width, frame_height);
  
  if ( dc1394_start_iso_transmission( handle, camera.node )
	!=DC1394_SUCCESS ) 
   {
      fprintf( stderr, "Unable to start camera iso transmission\n" );
      stop_stream();
      return 1;
   }
  return 1;
}

long Sequencer1394::grab_frame(){
  
  int failures = 0;
  while ( dc1394_dma_single_capture( &camera ) != DC1394_SUCCESS ){
    ++failures;
    // just continue despite frame capture failures,
    // retrying until a frame is successfully captured or
    // too many failures are recorded
    if(failures == FRAME_CAPTURE_MAX_FAILURES){
      stop_stream();
      exit(1);
    }
  }

  imlib_context_set_image(im);
  DATA32 *imgdata = imlib_image_get_data();
  int length = frame_width * frame_height;
  unsigned char *inptr = (unsigned char*)camera.capture_buffer;
  DATA32 *outptr = imgdata;
  for(unsigned int i=0; i < length; i+=4){

    *outptr = (*inptr<<16) | (*(inptr+1) << 8) | *(inptr+2);
    *(outptr+1) = (*(inptr+3) << 16) | (*(inptr+4) << 8) | *(inptr+5);
    *(outptr+2) = (*(inptr+6) << 16) | (*(inptr+7) << 8) | *(inptr+8);
    *(outptr+3) = (*(inptr+9) << 16) | (*(inptr+10) << 8) | *(inptr+11);
    
    inptr+=12;
    outptr+=4;
  }
  
  imlib_image_put_back_data(imgdata);
  dc1394_dma_done_with_buffer( &camera );

  ++current_frame;
  
  return current_frame;
}

int Sequencer1394::stop_stream(){
  if ( dc1394_stop_iso_transmission( handle,camera.node ) != DC1394_SUCCESS ) 
  {
    printf("Couldn't stop the camera?\n");
  }
  dc1394_dma_unlisten( handle, &camera );
  dc1394_dma_release_camera( handle, &camera );
  raw1394_destroy_handle( handle );

  if(imlib_context_get_image() != NULL){
    imlib_free_image_and_decache();
  }
}


Imlib_Image *Sequencer1394::get_image(){
  return &im;
}

















