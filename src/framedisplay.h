//    framedisplay.h - X display window for Xlib images
//
//    Copyright (C) 2005  Adam McCullough <adam.mccullough@mansfield.oxon.org>
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef CT_FRAMEDISPLAY_H
#define CT_FRAMEDISPLAY_H

#include <X11/Xutil.h>
#include <X11/extensions/shape.h>
#include <X11/Xlib.h>
#include <Imlib2.h>

namespace CamTrack{

  class FrameDisplay{

    public:
    
      FrameDisplay(unsigned int width, unsigned int height);
      ~FrameDisplay();
    
      int draw_frame(Imlib_Image *image);

    protected:

      Display *disp;
      Window win;
      Visual *vis;
      Colormap cm;

  };

};

#endif
