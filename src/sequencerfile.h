//    sequencerfile.h - frame sequencer for series of frames stored as files
//
//    Copyright (C) 2005  Adam McCullough <adam.mccullough@mansfield.oxon.org>
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#ifndef CT_SEQUENCER_FILE
#define CT_SEQUENCER_FILE

#include "sequencer.h"

namespace CamTrack{

  class SequencerFile: public Sequencer{

    public:
      SequencerFile(const char* filename);
      ~SequencerFile();
      
      int reset();
      int start_stream();
      long grab_frame();
      int stop_stream();
      
      Imlib_Image *get_image();

    protected:
    
      Imlib_Image im;
      char filename[128];
      char fileprefix[128];
      char filesuffix[128];
      unsigned long first_frame;
      unsigned long last_frame;
      unsigned long current_frame;
      char current_filename[128];
      
      const char* get_current_filename();
     
      unsigned int numdigits;
      
  };

};

#endif
