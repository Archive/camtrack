//    framedisplay.cc - X display window for Xlib images
//
//    Copyright (C) 2005  Adam McCullough <adam.mccullough@mansfield.oxon.org>
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


#include "framedisplay.h"

using namespace CamTrack;

FrameDisplay::FrameDisplay(unsigned int width, unsigned int height){

  disp = XOpenDisplay(NULL);
  // set up the X window
  vis = DefaultVisual(disp, DefaultScreen(disp));
  cm = DefaultColormap(disp, DefaultScreen(disp));
  
  win=XCreateSimpleWindow(disp, DefaultRootWindow(disp), 0, 0, width, height, 0, 0, 0);
  XSelectInput(disp,win,ButtonPressMask);

  XMapWindow(disp,win);
  
  imlib_context_set_display(disp);
  imlib_context_set_visual(vis);
  imlib_context_set_colormap(cm);
  imlib_context_set_drawable(win);
  imlib_context_set_blend(0);
  imlib_context_set_color_modifier(NULL);
  imlib_context_set_blend(0);
  
}

FrameDisplay::~FrameDisplay(){
}

int FrameDisplay::draw_frame(Imlib_Image *image){

  XSync(disp, False);
  imlib_context_set_image(*image);
  imlib_render_image_on_drawable(0, 0);

  return 1;
}












