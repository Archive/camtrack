// histogram.cc - build, load and normalize histograms
//
// Copyright (C) 2005 Adam McCullough <adam.mccullough@mansfield.oxon.org>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <iostream>
#include <string.h>

#include "float_cast.h"
#include "histogram.h"

using namespace CamTrack;

/*!
 *  \brief Construct the histogram and allocate storage.
 *
 *         Calculates the required storage for a histogram of the
 *         specified resolution with the specified range and allocates
 *         a table of the appropriate size.
 */
Histogram::Histogram(int p_minval, int p_maxval,
                     int p_resolution){
  init(p_minval, p_maxval, p_resolution);
}

/*!
 *  \brief Load the histogram from an input stream.
 *
 */
Histogram::Histogram(std::istream& input){
  
  char linebuffer[256] = "";
  char version[32] = "0.0";
  
  maxval = minval = 0;
  resolution = 0;
  bin_size = 0.0;
  
  bool header_done = false;
  
  // check that the stream identifies itself correctly
  // and load the header info
  while( (!input.eof()) && (!header_done)){
    
    input.getline(linebuffer, 256);
        
    if(strncmp(linebuffer, "#", 1)==0){
      // ignore comments
    }else if(strlen(linebuffer)==0){
      // ignore blank lines
    }else if(strncmp(linebuffer, "histogram version=", 18)==0){
      sscanf(linebuffer, "histogram version=%s", version);
    }else if(strncmp(linebuffer, "range=", 6)==0){
      sscanf(linebuffer, "range=%i-%i", &minval, &maxval);
    }else if(strncmp(linebuffer, "resolution=", 11)==0){
      sscanf(linebuffer, "resolution=%i", &resolution);
    }
    else if(strncmp(linebuffer, "table data:", 11)==0){
      header_done = true;
    }
  }
  
  if( (strncmp(version, "0.0", 3)==0) || (maxval==minval) || (resolution==0) ){
    std::cerr << "Incomplete or invalid header." << std::endl;
    exit(1);
  }
  
  bin_size = static_cast<float>(maxval - minval) / static_cast<float>(resolution);
  inv_bin_size = 1.0 / bin_size;
  
  #ifdef DEBUG
  std::cerr << "Loading histogram file, version " << version << std::endl;
  std::cerr << "Range      : " << minval << "-" << maxval << std::endl;
  std::cerr << "Resolution : " << resolution << std::endl;  
  #endif

  // allocate the histogram table
  table = new unsigned int[resolution];

  binlookup = new unsigned int[maxval+1];
  for(int i = 0; i <= maxval; ++i){
    binlookup[i] = lrintf( roundf(static_cast<float>(i) * inv_bin_size) );
  }
  
  // populate the histogram table
  unsigned int i = 0;
  int val = 0;
  while( !input.eof() ){
    
    input.getline(linebuffer, 256);
    
    if(strncmp(linebuffer, "#", 1)==0){
      // ignore comments
    }else if(strlen(linebuffer)==0){
      // ignore blank lines
    }else{
      sscanf(linebuffer, "%i:%i", &i, &val);
      if(i<resolution){
        #ifdef DEBUG
        std::cerr << "Setting value of bin " << i << " to " << val << std::endl;
        #endif
        table[i] = val;
      }
      
    }
  }
}

Histogram::Histogram(std::istream& input, int p_minval, int p_maxval, int p_resolution){
  init(p_minval, p_maxval, p_resolution);
  
  // count the stream
  count_stream(input);
}


Histogram::~Histogram(){
  //deallocate the histogram table
  delete [] table;
  delete [] binlookup;
}

void Histogram::init(int p_minval, int p_maxval,
                     int p_resolution){
  maxval = p_maxval;
  minval = p_minval;
  resolution = p_resolution;
  
  bin_size = static_cast<float>(maxval - minval) / static_cast<float>(resolution);
  inv_bin_size = 1.0 / bin_size;
  
  #ifdef DEBUG
    std::cerr << "Constructing Histogram, " << std::endl;
    std::cerr << "range : " << minval << "-" << maxval << std::endl;
    std::cerr << "resolution : " << resolution << std::endl;
    std::cerr << "bin size : " << bin_size << std::endl;
    
  #endif

  // allocate the histogram table
  table = new unsigned int[resolution];
  
  binlookup = new unsigned int[maxval+1];
  for(int i = 0; i <= maxval; ++i){
    binlookup[i] = lrintf( roundf(static_cast<float>(i) * inv_bin_size) );
  }
}


/*!
 *  \brief Add the given data point to the histogram
 *
 *         Increments the count for the bin corresponding to
 *         the given point.
 */
unsigned int Histogram::count(int point){
  unsigned int i = lrintf( static_cast<float>(point) / bin_size );
  return (*get_bin_by_index(i))++;
}


/*!
 *  \brief Applies the given value to the bin specified by
 *         the given coordinate vector
 */
unsigned int Histogram::set_bin_value(unsigned int bin, unsigned int val){
  table[bin] = val;
  return 1;
}

/*!
 *  \brief Returns the value of the specified bin
 */
unsigned int Histogram::get_bin_value(unsigned int bin){
  return table[bin];
}

/*!
 *  \brief Returns a pointer to the histogram bin specified by
 *         the given coordinate vector
 */
unsigned int *Histogram::get_bin_by_index(unsigned int index){
  #ifdef DEBUG
  std::cerr << "Bin index : " << index << std::endl;
  #endif
  
  return &(table[index]);
}

/*!
 *  \brief Read data points from an input stream and
 *         count them into the histogram
 */
unsigned int Histogram::count_stream(std::istream& input){
  char linebuffer[256] = "";
  unsigned int points_counted = 0;
  unsigned int point;
  while( !input.eof() ){
    input.getline(linebuffer, 256);
    
    if(strncmp(linebuffer, "#", 1)==0){
      // ignore comments
    }else if(strlen(linebuffer)==0){
      // ignore blank lines
    }else{
      sscanf(linebuffer, "%i", &point);
      count(point);
      ++points_counted;
    }
  }
  
  return points_counted;
}

unsigned int Histogram::write(std::ostream& output){
  // write header
  output << "histogram version=" << HISTOGRAM_VERSION << std::endl;
  output << "range=" << minval << "-" << maxval << std::endl;
  output << "resolution=" << resolution << std::endl;
  output << "#" << std::endl << "#" << std::endl;
  
  output << "table data:" << std::endl;
  
  unsigned int bins_written = 0;
  
  for(int i=0; i < resolution; ++i){
   if(table[i] != 0){
    output << i << ":" << table[i] << std::endl;
    ++bins_written;
   }
  }
  
  return bins_written;
}


int Histogram::get_minval(){return minval;}
int Histogram::get_maxval(){return maxval;}
int Histogram::get_resolution(){return resolution;}
















