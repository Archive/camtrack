//    probmap.cc - probability map
//
//    Copyright (C) 2005  Adam McCullough <adam.mccullough@mansfield.oxon.org>
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#include <iostream>
#include <math.h>

#include <config.h>

#include "float_cast.h"
#include "probmap.h"

using namespace CamTrack;

ProbMap::ProbMap(unsigned int w, unsigned int h)
:
width(w),
height(h){
  
  row_size = (width/32)+2;
  buffersize = row_size*(height+64);
  
  buffer1 = new uint32_t[buffersize];
  buffer2 = new uint32_t[buffersize];
  
  srcmap = new uint32_t*[height];
  dstmap = new uint32_t*[height];
  
  unsigned int offset = (row_size*32) + 1;
  
  for(int i=0; i<height; ++i){
    srcmap[i] = buffer1 + offset;
    dstmap[i] = buffer2 + offset;
    offset += row_size;
  }
}

ProbMap::~ProbMap(){
  delete [] srcmap;
  delete [] dstmap;
  delete [] buffer1;
  delete [] buffer2;
}
    
int ProbMap::scan(Imlib_Image *image, Histogram *hist, TrackingWindow &win){
  imlib_context_set_image(*image);
  register uint32_t val;
  float h;
  float sv;
  int a;
  
  int l = ((win.get_left()>>5)-2)<<5;
  if(l < 0) l = 0;
  int r = ((win.get_right()>>5)+2)<<5;
  if(r > width) r = width;
  int t = (win.get_top()-32);
  if(t < 0) t = 0;
  int b = (win.get_bottom()+32);
  if(b > height) b = height;
  
  for(unsigned int y=t; y<b; ++y){
    for(unsigned int x=l; x<r; x+=32){
    
      val = 0x00000000;
      
      imlib_image_query_pixel_hsva(x, y, &h, &sv, &sv, &a);
      
      h *= (!isnan(h)) * 2.55;
      val += hist->get_value_by_point((int)lrintf(h));
      
      for(unsigned int i=1; i < 32; ++i){
        
        val <<= 1;
        imlib_image_query_pixel_hsva(x+i, y, &h, &sv, &sv, &a) ;
        
        h *= (!isnan(h)) * 2.55;
        val += hist->get_value_by_point((int)lrintf(h));
      }
      srcmap[y][x/32] = val;
    }
  }
}


int ProbMap::close(){
  erode();
  swap_buffers();
  dilate();
  swap_buffers();
}

void ProbMap::swap_buffers(){
  uint32_t *tmp = buffer2;
  buffer2 = buffer1;
  buffer1 = tmp;
}

void ProbMap::set_eye_region(TrackingWindow &win){
  bool finished = false;
  int y = win.get_top();
  int x;
  unsigned int a;
  
  while(!finished){
    a = 0;
    for(x = 0; x<20; ++x){
      a += (get_pixel(x-10+win.get_centre_x(), y)>0);
    }
    
    if(a>12) finished = true;
    else if(y > win.get_centre_y()) finished = true;
    else ++y;
  }

  eyeregion_t = y+win.get_height()/5;

  finished = false;
  x = win.get_left();
  
  while(!finished){
    a = 0;
    
    for(y = win.get_centre_y()-(win.get_height()/4); y<win.get_centre_y(); ++y){
      a += (get_pixel(x, y)>0);
    }
    
    if(a>(win.get_height()/8)) finished = true;
    else if(x > win.get_centre_x()) finished = true;
    else ++x;
  }
  
  eyeregion_l = x;

  finished = false;
  x = win.get_right();
  
  while(!finished){
    a = 0;
    for(y = win.get_centre_y()-(win.get_height()/4); y<win.get_centre_y(); ++y){
      a += (get_pixel(x, y)>0);
    }
    
    if(a>(win.get_height()/8)) finished = true;
    else if(x < win.get_centre_x()) finished = true;
    else --x;
  }
  
  eyeregion_r = x;
}

// The following two methods are slightly modified forms of code from
// Leptonica (http://www.leptonica.com/), who provide a library
// of a number of image processing operations. Their code is
// extremely fast - beating my first naive testing implementation
// by a factor of 30 or more. The following notice applies here.

/*====================================================================*
-  Copyright (C) 2001 Leptonica.  All rights reserved.
-  This software is distributed in the hope that it will be
-  useful, but with NO WARRANTY OF ANY KIND.
-  No author or distributor accepts responsibility to anyone for the
-  consequences of using this software, or for whether it serves any
-  particular purpose or works at all, unless he or she says so in
-  writing.  Everyone is granted permission to copy, modify and
-  redistribute this source code, for commercial or non-commercial
-  purposes, with the following restrictions: (1) the origin of this
-  source code must not be misrepresented; (2) modified versions must
-  be plainly marked as such; and (3) this notice may not be removed
-  or altered from any source or modified source distribution.
*====================================================================*/

int ProbMap::dilate(){
  uint32_t *sptr, *dptr;
  uint32_t *tptr1m, *tptr1p;
  
  unsigned int pwpls = (width + 31)/32;
  
  for (unsigned int i = 0; i < height; ++i){
    sptr = buffer1 + (i * row_size) + (32 * row_size) + 1;
    dptr = buffer2 + (i * row_size) + (32 * row_size) + 1;
    for (unsigned int j = 0; j < pwpls; ++j, ++sptr, ++dptr) {
      tptr1m = sptr - row_size;
      tptr1p = sptr + row_size;
      *dptr = *sptr | (*sptr << 1) | (*sptr >> 1) |
        (*(sptr - 1) << 31) | (*(sptr + 1) >> 31) |
        *tptr1m | (*tptr1m << 1) | (*tptr1m >> 1) |
        (*(tptr1m - 1) << 31) | (*(tptr1m + 1) >> 31) |
        *tptr1p | (*tptr1p << 1) | (*tptr1p >> 1) |
        (*(tptr1p - 1) << 31) | (*(tptr1p + 1) >> 31);
    }
  }
}

void ProbMap::erode(){
  uint32_t *sptr, *dptr;
  uint32_t *tptr1m, *tptr1p;
  
  unsigned int pwpls = (width + 31)/32;
    
  for(unsigned int i=0; i < height; ++i){
    sptr = buffer1 + (i * row_size) + (32 * row_size) + 1;
    dptr = buffer2 + (i * row_size) + (32 * row_size) + 1;
    
    for(unsigned int j=0; j < pwpls; ++j, ++sptr, ++dptr){
      tptr1m = sptr - row_size;
      tptr1p = sptr + row_size;
      
      *dptr = ((*sptr >> 1) | (*(sptr - 1) << 31)) &
        *sptr &
        ((*sptr << 1) | (*(sptr + 1) >> 31)) &
        ((*tptr1m >> 1) | (*(tptr1m - 1) << 31)) &
        *tptr1m &
        ((*tptr1m << 1) | (*(tptr1m + 1) >> 31)) &
        ((*tptr1p >> 1) | (*(tptr1p - 1) << 31)) &
        *tptr1p &
        ((*tptr1p << 1) | (*(tptr1p + 1) >> 31));
    }
  }
}

/*====================================================================*/

