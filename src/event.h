// event.h - event manager
//
// Copyright (C) 2005 Adam McCullough <adam.mccullough@mansfield.oxon.org>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef CT_EVENT
#define CT_EVENT

#include "window.h"


#define X_SMOOTHING_KERNEL_SIZE 3
#define Y_SMOOTHING_KERNEL_SIZE 3
#define Z_SMOOTHING_KERNEL_SIZE 10

#define SWING_TIMEOUT 1.0
#define MOVE_TIMEOUT 1.0
#define BUTTON_TIME 0.2


namespace CamTrack{

  class EventManager{
    
    public:
      EventManager(const unsigned int width, 
                   const unsigned int height, 
                   const char* fifoname);
      ~EventManager();
      
      void update(TrackingWindow &win);
        
    protected:
      
      void reset_fifo();
      
      unsigned int width, height;
      char fifo_name[128];
      int fifo;
      
      float x, dx, d2x;
      float y, dy, d2y;
      float z, dz, d2z;
      
      double tlast, dt;
      
      float x_history[X_SMOOTHING_KERNEL_SIZE];
      float y_history[Y_SMOOTHING_KERNEL_SIZE];
      float z_history[Z_SMOOTHING_KERNEL_SIZE];
      unsigned int x_index;
      unsigned int y_index;
      unsigned int z_index;
      
      int button_left, button_right;
      int swing_left, swing_right;
      float button_left_time, button_right_time;
      float swing_left_time, swing_right_time;
      
      int move_left, move_right;
      float left_time, right_time;
      int move_up, move_down;
      float up_time, down_time;
      
      bool state_changed;
        
  };

};

#endif
