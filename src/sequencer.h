// sequencer.h - frame sequencer base class
//
// Copyright (C) 2005 Adam McCullough <adam.mccullough@mansfield.oxon.org>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef CT_SEQUENCER
#define CT_SEQUENCER

#include <X11/Xlib.h>
#include <Imlib2.h>

namespace CamTrack{

  class Sequencer{
    
    public:
      Sequencer(){};
      virtual ~Sequencer(){};
      
      // sequencer control
      
      virtual int reset(){};
      virtual int start_stream(){};
      virtual long grab_frame(){};
      virtual int stop_stream(){};
      
      // accessor functions
      
      virtual Imlib_Image *get_image(){};
     
      unsigned int get_width();
      unsigned int get_height();
      
    protected:
    
      unsigned int frame_width;
      unsigned int frame_height;
    
  };

};

#endif
