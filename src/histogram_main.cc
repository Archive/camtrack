// histogram_main.cc - build, load and normalize histograms
//
// Copyright (C) 2005 Adam McCullough <adam.mccullough@mansfield.oxon.org>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software 
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


#include <config.h>

#include <iostream>
#include <fstream>
#include <getopt.h>
#include <string.h>

#include "histogram.h"

using namespace CamTrack;

int gopt_build = 0;
int gopt_load = 0;
int gopt_read = 0;

char g_filename[256] = "";
int g_minval = 0;
int g_maxval = 255;

int g_resolution = 128;


static struct option long_options[]={
  {"build", 0, &gopt_build, 1},              // construct a histogram from pixel data
                                             // given on std input (default)
                                             
  {"load", 1, &gopt_load, 1},                // load the histogram data stored in the
                                             // specified file, and add pixel data
                                             // from std input
  
  {"resolution", 1, NULL, 0},                // set histogram resolution
  
  {"read", 0, &gopt_read, 1},                // load histogram data from std input
  
  {"range", 1, NULL, 0},                     // set the range of values accepted for
                                             // each component
  
  {"help", 0, NULL, 0},                      // print help info
  
  {NULL, 0,0,0}
};

int get_options(int argc, char* argv[]){
  
  int opt_index = 0;
  while(getopt_long(argc, argv, "", long_options, &opt_index)>=0){
    switch(opt_index){
     case 1: // --load
        strcpy(g_filename, optarg);
        gopt_build = 0;
        break;
      case 2: // --resolution
        sscanf(optarg, "%i", &g_resolution);
        break;
      case 3: // --read
        gopt_build = 0;
        break;
      case 4: // --range
        sscanf(optarg, "%i-%i", &g_minval, &g_maxval);
        break;
      case 5: // --help
        std::cout
        << "Usage: histogram [options]" << std::endl
        << "Build or manipulate a histogram, then write it to std output." << std::endl        
        << "Options:" << std::endl
        << "  --help                  Display usage information" << std::endl

        << "  --build                 Build a histogram from data points given on std input" << std::endl
        << "                          Data points should be given one point per line." << std::endl
        
        << "  --load <file>           Load the specified histogram file, then add point data" << std::endl
        << "                          from std input, as above." << std::endl
        
        << "  --resolution <res>      Set the resolution of the histogram" << std::endl
        << "                          This is the number of bins covering the accepted range." << std::endl
        << "                          Default is 128." << std::endl

        << "  --read                  Load a histogram file from std input" << std::endl

        << "  --range <min-max>       Set the accepted range of values." << std::endl
        << "                          Default is 0-255." << std::endl

        << "For more information, please see the README file distributed with this software." << std::endl;
        exit(0);
        break;
      default:
        if(gopt_build == 0){
          std::cerr << "Please try \"histogram --help\" for usage details." << std::endl;
          exit(1);
        }
        break;
    }
    
  }
  return 0;
}


int main(int argc, char* argv[]){
  
  Histogram *hist = NULL;
  
  // parse and respond to command line
  get_options(argc, argv);
    
  // load existing histogram if necessary
  
  if(gopt_load){                      // load from specified file and
                                      // build from std input
    std::filebuf fb;
    fb.open(g_filename, std::ios::in);
    std::istream input(&fb);
    
    hist = new Histogram(input);
    
    fb.close();
    
    hist->count_stream(std::cin);
    
    
  }else if(gopt_read){                // load from std input
    hist = new Histogram(std::cin);
  }
  else if(gopt_build){                // build from std input
    hist = new Histogram(std::cin, g_minval, g_maxval, g_resolution);
    
  }else{
    std::cerr << "No input method specified. Try \"histogram --help\" for instructions." << std:: endl;
  }
    
  // write the histogram data to output
  hist->write(std::cout);
  
  delete hist;
  return 0;
}

